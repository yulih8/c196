package com.example.bookmanager;

/**
 * Created by PC on 2017-09-01.
 */

public class Book {
    String author;
    String title;
    int price;
    String isbn;
    String course;

    public Book(){
        this.author = "null";
        this.title = "null";
        this.price = 0;
        this.isbn = "null";
        this.course = "none ";
    }

    public Book(String author, String title, int price, String isbn, String course) {
        this.author = author;
        this.title = title;
        this.price = price;
        this.isbn = isbn;
        this.course = course;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public int getPrice() {
        return price;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getCourse() {
        return course;
    }


    public void setAuthor(String author) {
        this.author = author;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    @Override
    public boolean equals(Object obj) {

        if(obj == null){
            return false;
        }

        if(obj instanceof Book){
            Book otherBook = (Book) obj;
            return this.isbn == otherBook.isbn;
        } else {
            return false;
        }

    }
}
