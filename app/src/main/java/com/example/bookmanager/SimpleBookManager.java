package com.example.bookmanager;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.ArraySet;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.google.gson.*;

/**
 * Created by PC on 2017-09-01.
 */

public class SimpleBookManager implements BookManagerInterface {
    private Gson gson = new Gson();

    final protected static String TAG = "sbmMsg";
    final protected static String PREFS = "prefs";
    final protected static String LIB_KEY = "lib";
    private  ArrayList<Book> library = new ArrayList<>();

    public SimpleBookManager(SharedPreferences sharedPref) {
        loadData(sharedPref);
    }

    private void loadData(SharedPreferences sharedPref){
        Set<String> jsonLib = sharedPref.getStringSet(LIB_KEY, null);
        if(jsonLib != null){
            Log.i(TAG, "Loading saved books");
            for(String jsonStr : jsonLib){
                Book book = gson.fromJson(jsonStr, Book.class);
                library.add(book);
            }
        } else {
            Log.i(TAG, "Creating new books");
            for (int i = 0; i < 5; i++) {
                Book books = new Book("Author_" + i, "title_" + i, i, "isbn:" + i, "courses_" + i);
                library.add(books);
            }
        }

        printBooks(library);
    }

    @Override
    public int getMaxPrice() {
        int max = library.get(0).getPrice();
        for(Book b :  library) {
            if(b.price > max){
                max = b.price;
            }
        }
        return max;
    }

    @Override
    public int count() {
        return library.size();
    }

    @Override
    public Book getBook(int index) {
        return library.get(index);
    }

    @Override
    public Book createBook() {
        return new Book();
    }

    @Override
    public ArrayList<Book> getAllBooks() {
        return library;
    }

    @Override
    public void removeBook(Book book) {
        library.remove(book);
    }

    @Override
    public void moveBook(int from, int to) {
        Book bookToMove = library.remove(from);
        library.add(to, bookToMove);
    }

    @Override
    public int getMinPrice() {
        int min = library.get(0).getPrice();
        for(Book b :  library) {
            if(b.price < min){
                min = b.price;
            }
        }
        return min;
    }

    @Override
    public float getMeanPrice() {
        return (float) ( getTotalCost() / library.size() );
    }

    @Override
    public int getTotalCost() {
        int totalCost = 0;
        for(Book b :  library) {
            totalCost += b.getPrice();
        }
        return totalCost;
    }

    @Override
    public void saveChanges(SharedPreferences sharedPref) {
        Log.i(TAG, "Saving changes...");
        Set<String> jsonSet= new HashSet<>();
        for(Book b : library){
            String json = gson.toJson(b, Book.class);
            Log.i(TAG, json);
            jsonSet.add(json);
        }

        //save to sharedPrefs
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.putStringSet(LIB_KEY, jsonSet);
        editor.commit();

    }

    private static void printBooks(ArrayList<Book> library) {
        String str = "";

        for (int i = 0; i < library.size(); i++) {
            String mAuthor = library.get(i).getAuthor();
            String mTitle = library.get(i).getTitle();
            int mPrice = library.get(i).getPrice();
            String mIsbn = library.get(i).getIsbn();
            String mCourse = library.get(i).getCourse();

            str += "Book #" + Integer.toString(i+1) + "\n" +
                    "Author: " + mAuthor + "\n" +
                    "Title: " + mTitle + "\n" +
                    "Price: " + Integer.toString(mPrice) + "\n" +
                    "Isbn: " + mIsbn + "\n" +
                    "Course: " + mCourse + "\n\n";
        }

        Log.i(TAG, str);

    }
}
