package com.example.bookmanager;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class SummaryActivity extends AppCompatActivity {

    //Constants
    private static final String TAG = "SummaryActivity";

    //Views
    TextView totalCost;
    TextView mostExpItem;
    TextView leastExpItem;
    TextView avgPrice;

    //Model
    SimpleBookManager smb;
    Book book;
    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.summary);

        totalCost = (TextView) findViewById(R.id.tv_TotCost);
        mostExpItem = (TextView) findViewById(R.id.tv_MostExpItem);
        leastExpItem = (TextView) findViewById(R.id.tv_LeastExpItem);
        avgPrice = (TextView) findViewById(R.id.tv_AvgPrice);
    }

    @Override
    protected void onResume(){
        super.onResume();
        sharedPref = getApplicationContext().getSharedPreferences(SimpleBookManager.PREFS, Context.MODE_PRIVATE);
        smb = new SimpleBookManager(sharedPref);
        book = smb.getBook(0);
        updateGui();

    }

    @Override
    protected void onPause(){
        Log.i(TAG, "onPause");
        super.onPause();
        //save data
        smb.saveChanges(sharedPref);
    }

    public void updateGui(){
        /*
        title.setText("Mobile Computing");
        author.setText("Andrew A Andrew ");
        course.setText("CIU196");
        price.setText("225 SEK");
        isbn.setText("123 45 6789");
        */
        if(smb != null){
            totalCost.setText(String.valueOf(smb.getTotalCost()) + " kr");
            mostExpItem.setText(String.valueOf(smb.getMaxPrice()) + " kr");
            leastExpItem.setText(String.valueOf(smb.getMinPrice()) + " kr");
            avgPrice.setText(String.valueOf(smb.getMeanPrice()) + " kr");
        }

    }

}
