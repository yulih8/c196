package com.example.bookmanager;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(SimpleBookManager.PREFS, Context.MODE_PRIVATE);
        SimpleBookManager newSimpleLibrary = new SimpleBookManager(sharedPref);
    }
}
