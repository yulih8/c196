package com.example.bookmanager;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {
    //Constants
    private static final String TAG = "DetailActivity";

    //Views
    TextView title;
    TextView author;
    TextView course;
    TextView price;
    TextView isbn;

    //Model
    SimpleBookManager smb;
    Book book;
    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        title = (TextView) findViewById(R.id.tv_title);
        author = (TextView) findViewById(R.id.tv_Author);
        course = (TextView) findViewById(R.id.tv_Course);
        price = (TextView) findViewById(R.id.tv_Price);
        isbn = (TextView) findViewById(R.id.tv_Isbn);

    }

    @Override
    protected void onResume(){
        super.onResume();
        sharedPref = getApplicationContext().getSharedPreferences(SimpleBookManager.PREFS, Context.MODE_PRIVATE);
        smb = new SimpleBookManager(sharedPref);
        book = smb.getBook(0);
        updateGui();
    }

    @Override
    protected void onPause(){
        Log.i(TAG, "onPause");
        super.onPause();
        //save data on pause
        smb.saveChanges(sharedPref);
    }

    public void updateGui(){
        /*
        title.setText("Mobile Computing");
        author.setText("Andrew A Andrew ");
        course.setText("CIU196");
        price.setText("225 SEK");
        isbn.setText("123 45 6789");
        */
        if(book != null){
            title.setText(book.getTitle());
            author.setText(book.getAuthor());
            course.setText(book.getCourse());
            price.setText(book.getPrice() + " kr");
            isbn.setText(book.getIsbn());
        }

    }
}
